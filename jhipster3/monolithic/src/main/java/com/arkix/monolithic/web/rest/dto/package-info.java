/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.arkix.monolithic.web.rest.dto;
