(function() {
    'use strict';

    angular
        .module('monolithicdemoApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
