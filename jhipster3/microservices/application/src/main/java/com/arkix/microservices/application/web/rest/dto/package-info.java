/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.arkix.microservices.application.web.rest.dto;
