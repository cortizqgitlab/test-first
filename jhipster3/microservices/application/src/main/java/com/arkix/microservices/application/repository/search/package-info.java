/**
 * Spring Data ElasticSearch repositories.
 */
package com.arkix.microservices.application.repository.search;
