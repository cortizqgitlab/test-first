/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.arkix.microservices.gateway.web.rest.dto;
