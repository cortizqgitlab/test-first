(function() {
    'use strict';

    angular
        .module('gatewayappApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
